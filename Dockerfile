FROM python:slim-buster

COPY ./app /random_cat
RUN apt update && apt dist-upgrade -yy && \
	pip install -r /random_cat/requirements.txt

WORKDIR /random_cat

CMD gunicorn -b 0.0.0.0:8000 bebe_chat_app:app
