from random import randint
from flask import Flask, render_template

app = Flask(__name__)

TOTAL_REQUEST = 0

@app.route('/get_random_sentence')
def get_random_sentence():
    names = ('Xibao', 'Maobe', 'Meilv', 'Sesame')
    something = ('a zhu', 'cute', 'chou', 'lovely', 'lei', 'stingy', 'pink')

    i_name = randint(0, len(names) - 1)
    i_st = randint(0, len(something) - 1)
    return f'{names[i_name]} is {something[i_st]}'


@app.route('/')
def hello_world():
    global TOTAL_REQUEST

    s = get_random_sentence()
    nb_request = TOTAL_REQUEST
    TOTAL_REQUEST += 1

    return render_template('page.html', nb_request=nb_request,
                           sentence=s)
