from flask import Flask, render_template
from random import randint

app = Flask(__name__)

TOTAL_REQUEST = 0

@app.route('/get_random_sentence')
def get_random_sentence():
    names = ('Xibao', 'Maobe', 'Meilv', 'Sesame')
    content = ('a zhu', 'cute', 'chou', 'lovely', 'lei', 'stin\
gy', 'pink')
    name_id = randint(0, len(names) - 1)
    content_id = randint(0, len(content) - 1)
    return f'{names[name_id]} is {content[content_id]}'


@app.route('/')
def hello():
    global TOTAL_REQUEST
    s =  get_random_sentence()
    nb_request = TOTAL_REQUEST
    TOTAL_REQUEST += 1
    return render_template("page.html", nb_request=nb_request,
    sentence=s)

if __name__ == '__main__':

    app.run()
