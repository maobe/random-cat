from celery import Celery
from random import randint
from time import sleep

daigou_network = Celery('tasks',
                        backend='redis://localhost',
                        broker='redis://localhost//')


@daigou_network.task
def buy_bag(brand: str):
    print(f'Xiao doggy try to buy a {brand} bag')
    sleep(randint(0, 10))
    print(f'Xiao doggy baught a {brand} bag')
    return f'a {brand} bag'
