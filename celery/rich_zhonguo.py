from big_daigou import buy_bag
from time import sleep

brands = ['Hermes', 'Channel', 'dirty LV', 'CherryCoco']

orders = []
for brand in brands:
    print(f'Rich zhonguo is asking to buy a {brand} bag')
    orders.append(buy_bag.apply_async(args=[brand]))
