# Daigou

## Installation

### Install broker
```
sudo apt install redis
```

### Install python env
```
mkvirtualenv celery-test
pip install redis celery
```

## HowTo

### Start celery worker
```
celery -A big_daigou worker --loglevel=INFO
```

### Launch tasks
```
python rich_zhonguo.py
```
