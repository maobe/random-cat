import os

import requests
from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_user, logout_user, login_required
from bebe_chat_app import app, db
from bebe_chat_app.forms import LoginForm, RegisterForm, UserPost
from bebe_chat_app.models import User, Post
from werkzeug.urls import url_parse

@app.route('/guaibaobao')
def get_cat_url():
    response = requests.get('https://api.thecatapi.com/v1/images/search')
    cat_url = response.json()[0]['url']
    return cat_url


@app.route('/')
@app.route('/index')
@login_required
def index():
    my_info = {'username': 'Maobe', 'title': 'cat planet'}

    person = {
        'John': 'Beautiful day in Portland!',
        'Susan': 'The Avengers movie was so cool!',
        'Sesame': 'I LOVE U!'
    }
    contact = {
            'sesame': '06.88.88.88.88',
        'meilv': '888',
        'maobe': '666',
        'zhube': '555'
    }
    cat_url = get_cat_url()
    return render_template('index.html', posts=person, cats=contact, cat_url=cat_url)

#route name and name of function should be the same
@app.route('/my_cats', methods=['GET', 'POST'])
def my_cats():
    cat_url = get_cat_url()
    post = UserPost()
    if post.validate_on_submit():
        u = User.query.get(current_user.id)
        p = Post(body=post.content.data, author=u)
        db.session.add(p)
        db.session.commit()
        return render_template('new_cat_index.html', cat_url=cat_url, form=post)
    return render_template('new_cat_index.html', cat_url=cat_url, form=post)

def manage_login_user(login_form):
    user = User.query.filter_by(username=login_form.username.data).first()
    if user is None or not user.check_password(login_form.password.data):
        #flash('Invalid username or password')
        return redirect(url_for('login'))
    login_user(user, remember=True)
    return redirect(url_for('my_cats'))
#    next_page = request.args.get('next')
#   if not next_page or url_parse(next_page).netloc != '':
#      next_page = url_for('index')
# return redirect(next_page)

@app.route('/login', methods=['GET', 'POST'])
def login():
    login_form = LoginForm()
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    if login_form.validate_on_submit():
        return manage_login_user(login_form)
    return render_template('login.html', title='Sign In', form=login_form)


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    register_form = RegisterForm()
    if register_form.validate_on_submit():
        u = User(username=register_form.username.data,
                 email=register_form.email.data)
        u.set_password(register_form.password.data)
        db.session.add(u)
        db.session.commit()
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=register_form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))




# @app.route('/xibao')
# def nihao():
#     username = 'xibao'
#     return """
#     <html>
# <body>
# <h1>Bebe Chat blog</h1>
# <p>Hello {0}</p>
# </body>
# </html>
# """.format(username)
