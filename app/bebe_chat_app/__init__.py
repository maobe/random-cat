from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from flask import Flask
from bebe_chat_app.config import my_config
from flask_login import LoginManager
import logging
from logging.handlers import SMTPHandler

app = Flask(__name__)
app.config.from_object(my_config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
login.login_view = 'login'

from bebe_chat_app import routes, models, error
